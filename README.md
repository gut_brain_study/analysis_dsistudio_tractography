# Tractography in DSI Studio

This GitLab repository documents the fibre tracking of the uncinate fasciculus (UF), a sub-bundle of the UF and of the whole brain in the Gut-Brain Study. 

* Software version 2022-01-11
* scripts to run tractography pipeline: 
  * tracing of (sub-)UF: ./Step1-6_run_dsi_studio.sh 
  * tracing whole brain: ./Step4-6_run_dsi_studio_whole_brain.sh 
  * tables with results: ./Step_7_...
* from dsi-studio website (http://dsi-studio.labsolver.org/Manual/command-line-for-dsi-studio), I copied all available options for tractography to: ./options_for_dsi_studio.sh

## 1. Reconstruction using GQI: selection of diffusion sampling length ratio L:

* tried out values between 0.4 and 3.0 (folder: choice_of_L/)
* optimal value should resolve crossing patterns in crossing regions and still have clean fiber direction at mid corpus callosum 
* seems to be the case for L=2.0

## 2. Fiber tracking: 

### 2a. Creation and selection of regions:

dimensions of DWI files: 78,94,68

reference T1:
analysis_dsistudio_tractography/standard_spaces_regions/MNI152_T1_1mm.nii.gz

templates, atlases and regions available in folders: 

* regions_for_DSIstudio/
* choice_of_regions_and_parameters/


#### **2a.1 Tracing of entire UF:**

**Seed region:** 

from JHU DTI-based white matter atlas 1mm (according to Granger et al. 2021): 

* JHU-ICBM white-matter labels atlas added manually to DSI studio atlas folder: regions_for_DSIstudio/standard_templates_and_atlases/JHU-ICBM-labels-1mm.nii.gz
* and the according labels: regions_for_DSIstudio/standard_templates_and_atlases/JHU-labels.xml

* in bash script: --seed=JHU-ICBM-labels-1mm:Uncinate_fasciculus_L (for right hemisphere respectively)

**End region:**

from Granger et al. 2021:

merged Brodman Areas 11 (Orbitofrontal area) and 47 (Orbital part of inferior frontal gyrus) from DSI Studio version 2017/07/26 --> Brodman Areas 11 and 47 are different in version 2022/01/11 used for this analysis 

added: BA10 from DSI Studio version 2022/01/11 --> include also frontal areas (Hau et al. 2017)

* nifti from Granger:regions_for_DSIstudio/from_sgranger/Granger_End_BA_11_47.nii.gz
* merged regions: regions_for_DSIstudio/manually_created/End_Granger_BA_11_47_and_BA_10.nii.gz
* in bash script: --end=$regions_dir/from_sgranger/Granger_End_BA_11_47.nii.gz+Brodmann:BA10_L (for tight hemisphere respectively)

**Regions of Avoidance (ROAs):**

adapted from Granger et al. 2021: axial ROA moved higher up to include frontal areas and blocks instead of planes only

* block superior to UF: regions_for_DSIstudio/manually_created/ROA_superior_a41-67_c2-46.nii.gz
* block posterior to UF: regions_for_DSIstudio/manually_created/ROA_posterior_c46-93.nii.gz
* left frontal block for tracking of right UF: regions_for_DSIstudio/manually_created/ROA_L_a4-40_s39-77_c2-46.nii.gz
* right frontal block for tracking of left UF: regions_for_DSIstudio/manually_created/ROA_R_a4-40_s0-39_c2-46.nii.gz
* sphere: regions_for_DSIstudio/manually_created/ROA_sphere_a16-28_s32-45_c31-44.nii.gz --> further back and a bit smaller than Granger's; with Granger's sphere some medial fibres of the UF were lost


#### **2a.2 Tracing of bundle connecting OFC and Amygdala+Entorhinal cortex:**

**Seed region:**

from automated anatomical labeling atlas (AAL2) from the Neurofunctional Imaging Group at the University of Bordeaux (https://www.gin.cnrs.fr/en/tools/aal/) implemented in DSI Studio version 2022/01/11

anterior, lateral, medial and posterior OFC for each hemisphere separately:

* regions_for_DSIstudio/standard_templates_and_atlases/Seed_AAL2_OFC_L.nii.gz  (for right hemisphere respectively)
* in bash script: --seed=AAL2:OFCmed_L+AAL2:OFCant_L+AAL2:OFCpost_L+AAL2:OFClat_L (for right hemisphere respectively)

**End region:**

Amygdala from FreeSurferDKT subcortical atlas and entorhinal cortex from FreesurferDKT cortical atlas within DSI Studio version 2022/01/11

* regions_for_DSIstudio/standard_templates_and_atlases/End_FreeSurferSeg_L_Amygdala_FreeSurferDKT_entorhinal.nii.gz (for right hemisphere respectively)
* in bash script: --end=FreeSurferDKT_Cortical:left_entorhinal+FreeSurferDKT_Subcortical:left_amygdala (for right hemisphere respectively)

**ROAs:**

same as for tracing of entire UF

#### **2a.3 Tracing of whole brain:**

no region specifications needed


#### **other regions I tried out:**

from https://my.vanderbilt.edu/tractem/2018/04/20/uncinate-fasciculus-unc/ (downloaded in folder: from_VUMC/)

* seed region on axial slice 12
* ROAs


**Seed region from JHU DTI-based white matter atlas 1mm:**

JHU white-matter tractography atlas (with probability threshold=25%)

* index=17: Uncinate fasciculus R
* index=18: Uncinate fasciculus L

**Manually created seed region with UF ROI from HCP842-tractography atlas**

* on axial slices 12 to 16

### 2b. Selection of angular threshold:

* tried out: random, 55, 65 and 75 degrees (folder: choice_of_regions_and_parameters/angular_threshold)
* 65 degrees seems to provide a balance between spurious fibres and correct streamlines
* for whole brain: turning angle = 0

### 2c. Selection of other parameters:

* for tracking of entire UF: seed_count=1,000,000 (default); for tracking of sub-bundle and whole brain: seed_count=10,000,000 (due to much larger seed region)
* step_size=0 (meaning random)
* min_length=0 
* for (sub-)UF: max_length=150mm according to Granger et al. (2021); for whole brain=300mm
* thread_count = 16 (amount of available CPU cores)

### 3. Tracking results:

**Tracts**

/data/pt_02020/MRI_data/DWI_results/tracking_UF/tracts
/data/pt_02020/MRI_data/DWI_results/tracking_whple_brain/tracts

**Tract statistics**

/data/pt_02020/MRI_data/DWI_results/tracking_UF/analysis_of_tracts
/data/pt_02020/MRI_data/DWI_results/tracking_whole_brain/analysis_of_tracts



## Processing History

**2022-01-26**

* **Step 0c** copying input data into folder: /data/pt_02020/MRI_data/wd/DWI/input_dsistudio/
* **Step 1** creation of src files: /data/pt_02020/MRI_data/wd/DWI/src/
* **Step 2a** Quality control results: /data/pt_02020/MRI_data/wd/DWI/src/qc.txt 
    * 7 outliers detected which are consistent with the outliers of EDDY QC: /data/pt_02020/MRI_data/scripts/2_DWI/0_preproc/qa/single_subject_results.xlsx
    * outliers of EDDY QC have already been visually checked and decided on regarding inclusion or exclusion (see preregistration here: https://osf.io/edu5x/ )
    * exclusions:
      * sub-01 --> pilot subject
      * sub-09_ses-02 --> DWI quality not sufficient
      * sub-29_ses-04 --> DWI quality not sufficient
      * sub-30_ses-01 --> DWI quality not sufficient
      * sub-34 --> neurological disorder detected post-acquisition
      * sub-45_ses-01 --> DWI quality not sufficient
* **Step 2b** moving of excluded data sets into folder: /data/pt_02020/MRI_data/wd/DWI/excluded_src
* **Step 3** reconstruction using GQI; fib files: /data/pt_02020/MRI_data/wd/DWI/src/fib
* **Step 4** fiber tracking of whole and sub-bundle of UF

**2022-01-27**

* **Step 5** tract-specific analysis to get measures such as number of tracts and nQA
* **Step 6** copy tracts and tract statistics into results folder: /data/pt_02020/MRI_data/DWI_results/tracking_UF/

**2022-01-28**

* **Step 7** copy all tract statistics from single files into one table /data/pt_02020/MRI_data/DWI_results/tracking_UF/tract_statistics.csv

**2022-03-21**

* **Step 4** fiber tracking of whole brain
* **Step 5** whole brain analysis to get measures such as number of tracts and nQA
* **Step 6** copy whole brain tracts and tract statistics into results folder: /data/pt_02020/MRI_data/DWI_results/tracking_whole_brain/
* **Step 7** copy all tract statistics from single files into one table /data/pt_02020/MRI_data/DWI_results/tracking_whole_brain/tract_statistics_whole_brain.csv
