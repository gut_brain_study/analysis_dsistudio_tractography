#!/bin/bash

##########################################################################################################
#					User Settings
##########################################################################################################
create_settings="
--seed=/mnt/yassamri/Conte_Center/DTIregions/UF_Fornix_analysis/Uncinate_fasciculus_L.nii.gz
--end=/mnt/yassamri/Conte_Center/DTIregions/UF_Fornix_analysis/11_47.nii.gz
--roa=/mnt/yassamri/Conte_Center/DTIregions/UF_Fornix_analysis/ROA.nii.gz

--method=0 
--seed_count=500000
--threshold_index=qa 
--fa_threshold=0.00
--initial_dir=0
--seed_plan=0
--interpolation=0
--thread_count=12
--step_size=0
--turning_angle=55
--smoothing=.6
--min_length=0
--max_length=150
--output=L_UF.trk.gz
";


analyze_settings="
--tract=L_UF.trk.gz
--output=L_UF.txt
--export=stat
";

#########################################################################################################
#########################################################################################################
#########################################################################################################



create_src_settings="
--action=trk 
--source=";

analyze_src_settings="
--action=ana
--source=";






# Loop through all the directories
for d in 857_2_MRI2/ ; do

	echo
	# Change directory into that directory -- needed for DSI_Studio
	pushd $d
	cd NIFTI
	echo Starting DSI_Studio script create and analyze for folder: $d ...

	# Find the .fib.gz file
	arr=( $(find *fib.gz -type f) )	

	# If there is no .fib.gz file, then continue
	if [ -z "$arr" ] ; then
		echo $d has no *.fib.gz file. Continuing with the next directory...

	# Otherwise, there is a .fib.gz file, lets run dsi studio
	else
		# Run dsi studio with create settings
		echo 'Running DSI Studio Create script...'
		/tmp/mribin/dsistudio/dsi_studio_Jul26_2017 $create_src_settings$arr$create_settings
		echo 'Create complete.'

		# Run dsi studio with analyze settings
		echo 'Running DSI Studio Analyze script...'
		/tmp/mribin/dsistudio/dsi_studio_Jul26_2017 $analyze_src_settings$arr$analyze_settings
		echo 'Analysis complete.'
	fi

	echo
	popd

done

