#! /bin/bash

# author: thieleking@cbs.mpg.de
#created on 2022-01-26

results_dir="/data/pt_02020/MRI_data/DWI_results/tracking_UF"
ana_dir="/data/pt_02020/MRI_data/DWI_results/tracking_UF/analysis_of_tracts"

ana_files=$(ls $ana_dir)

echo "subj ses tract number_of_tracts mean_length(mm) span(mm) curl elongation diameter(mm) volume(mm^3) trunk_volume(mm^3) branch_volume(mm^3) total_surface_area(mm^2) total_radius_of_end_regions(mm) total_area_of_end_regions(mm^2) irregularity area_of_end_region_1(mm^2) radius_of_end_region_1(mm) irregularity_of_end_region_1 area_of_end_region_2(mm^2) radius_of_end_region_2(mm) irregularity_of_end_region_2 qa nqa odf1 dti_fa md ad rd iso rdi nrdi02L nrdi04L nrdi06L" > $results_dir/tract_statistics.csv

for ana_file in $ana_files
do
    echo $ana_file
    subj=$(echo $ana_file | cut -d'_' -f1)
    echo $subj
    ses=$(echo $ana_file | cut -d'_' -f2) 
    echo $ses
    part=$(echo $ana_file | cut -d'.' -f8)
    tract=$(echo ${part:3}) ### remove "gz_" from beginning of tract name
    echo $tract
    
    values=""
    i=1
    while read line; do
        set -- $line
        value=$(echo ${@: -1:1})
        key=$(echo ${@: 1:1})
        echo $value
        echo $key
        if [[ $i != 22 ]]
        then 
            values="$values $value"
        elif [[ $i == 22 && $key == "odf1" ]]
        then
            values="$values $value" ### append single statistic values into one array
        else 
            values="$values NA $value"    ### some subjects don't have "odf1" in line 22 --> NA for odf1 and value for dti_fa
        fi
        i=$(($i+1))
    done < $ana_dir/$ana_file
    ### print whole array into tract statistics table 
    echo $subj $ses $tract $values >> $results_dir/tract_statistics.csv
done

