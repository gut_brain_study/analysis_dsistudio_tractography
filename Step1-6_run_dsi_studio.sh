#! /bin/bash

# author: thieleking@cbs.mpg.de
#created on 18th Oct 21
# testing phase over on 2022-01-25 -> start of main analysis

### ### Step 0a:  connect to compute server with cuda support
# getserver -g6 -sL --cuda=x
### ### Step 0b: choose version, start shell and mount paths
# singularity shell --nv -B /data/pt_02020 /data/p_SoftwareServiceLinux_sc/dsistudio/2022.01.11_JHU-ICBM-labels-1mm/1

##### TODO: Define directories

##### originals directories
input_dir_eddy="/data/pt_02020/MRI_data/DWI_preprocessed/eddy" ## take nifti and bvecs from here: e.g. /data/pt_02020/MRI_data/DWI_preprocessed/eddy/sub-04/ses-02/eddy_corrected.nii.gz and /data/pt_02020/MRI_data/DWI_preprocessed/eddy/sub-04/ses-02/eddy_corrected.eddy_rotated_bvecs
input_dir_bvals="/data/pt_02020/MRI_data/BIDS" ## e.g. /data/pt_02020/MRI_data/BIDS/sub-04/ses-02/dwi/sub-04_ses-02_dwi.bval

#### working directories
input_dir="/data/pt_02020/MRI_data/wd/DWI/input_dsistudio"
test_dir="/data/pt_02020/MRI_data/wd/DWI/test_input"
src_dir="/data/pt_02020/MRI_data/wd/DWI/src"
test_src_dir="/data/pt_02020/MRI_data/wd/DWI/test_src"
exclusions_dir="/data/pt_02020/MRI_data/wd/DWI/excluded_src"
regions_dir="/data/pt_02020/Analysis/analysis_dsistudio_tractography/regions_for_DSIstudio"
regions_dir_granger="/data/pt_02020/Analysis/analysis_dsistudio_tractography/from_sgranger"

#### final results directory
results_dir="/data/pt_02020/MRI_data/DWI_results/tracking_UF"

# echo "#####"
# echo "Step 0c: copy files into input directory"
# echo "#####"
# 
# rm -r $input_dir
# mkdir $input_dir
# 
# subjects=$(ls $input_dir_eddy)
# for sub in $subjects
# do
#     echo 
#     echo $sub
#     mkdir $input_dir/${sub}
# 
#     sessions=$(ls $input_dir_eddy/$sub)
#     for ses in $sessions
#     do
#         echo $ses
#         mkdir $input_dir/${sub}/${ses}
#         cp $input_dir_eddy/$sub/$ses/eddy_corrected.nii.gz $input_dir/${sub}/${ses}/${sub}_${ses}_dwi.nii.gz
#         cp $input_dir_eddy/$sub/$ses/eddy_corrected.eddy_rotated_bvecs $input_dir/${sub}/${ses}/${sub}_${ses}_dwi.eddy_rotated_bvecs.txt
#         cp $input_dir_bvals/$sub/$ses/dwi/*.bval $input_dir/${sub}/${ses}/${sub}_${ses}_dwi.bval.txt
#     done
# done



# echo "#####"
# echo "Step 1: create src files"
# echo "#####"
# 
# ### for complete analysis:
# subjects=$(ls $input_dir)
# for sub in $subjects
# do
#     echo $sub
#     sessions=$(ls $input_dir/$sub)
#     for ses in $sessions
#     do
#         echo $ses
#         dsi_studio --action=src --source=$input_dir/$sub/$ses/${sub}_${ses}_dwi.nii.gz --bval=$input_dir/$sub/$ses/${sub}_${ses}_dwi.bval.txt --bvec=$input_dir/$sub/$ses/${sub}_${ses}_dwi.eddy_rotated_bvecs.txt --up_sampling=0 --output=$src_dir 
#     done
# done

# # # ### for testing:
# # # subjects=$(ls $test_dir)
# # # for sub in $subjects
# # # do        
# # #     echo
# # #     echo $sub
# # #     sessions=$(ls $test_dir/$sub)
# # #     for ses in $sessions
# # #     do
# # #         echo $ses
# # #         dsi_studio --action=src --source=$test_dir/$sub/$ses/${sub}_${ses}_dwi.nii.gz --bval=$test_dir/$sub/$ses/${sub}_${ses}_dwi.bval.txt --bvec=$test_dir/$sub/$ses/${sub}_${ses}_dwi.eddy_rotated_bvecs.txt --up_sampling=0 --output=$test_src_dir 
# # #         echo
# # #     done
# # # done


# echo "#####"
# echo "Step 2a: Quality control"
# echo "#####"
# 
# ### for complete analysis:
# dsi_studio --action=qc --source=$src_dir

# # # ### for testing:
# # # dsi_studio --action=qc --source=$test_src_dir

# echo "#####"
# echo "Step 2b: move excluded subjects"
# echo "#####"
# 
# mv $src_dir/sub-01* $exclusions_dir          #pilot subject
# mv $src_dir/sub-09_ses-02* $exclusions_dir   #DWI quality not sufficient
# mv $src_dir/sub-29_ses-04* $exclusions_dir   #DWI quality not sufficient
# mv $src_dir/sub-30_ses-01* $exclusions_dir   #DWI quality not sufficient
# mv $src_dir/sub-34* $exclusions_dir          #neurological disorder detected post-acquisition
# mv $src_dir/sub-45_ses-01* $exclusions_dir   #DWI quality not sufficient


# echo "#####"
# echo "Step 3: Reconstruction using GQI"
# echo "#####"
# 
# # src_files=$(ls $test_src_dir/*.src.gz)
# src_files=$(ls $src_dir/*.src.gz)
# 
# # mkdir -p $test_src_dir/fib
# mkdir -p $src_dir/fib
# 
# for src_file in $src_files
# do
#     echo
#     dsi_studio --action=rec --source=$src_file --method=4 --param0=2.0 --record_odf=1 --output=$src_dir/fib
#     echo
# done
# 
# 
# echo "#####"
# echo "Step 4: Fiber tracking"
# echo "#####"
# 
# tracking_settings="
# --method=0 
# --roa=$regions_dir/manually_created/ROA_superior_a41-67_c4-46.nii.gz
# --roa2=$regions_dir/manually_created/ROA_posterior_c46-93.nii.gz
# --roa3=$regions_dir/manually_created/ROA_sphere_a16-28_s32-45_c31-44.nii.gz
# --threshold_index=qa 
# --fa_threshold=0.00 
# --initial_dir=0 
# --seed_plan=0 
# --interpolation=0 
# --thread_count=16 
# --step_size=0 
# --turning_angle=65 
# --smoothing=0.6 
# --min_length=0 
# --max_length=150 
# ";
# 
# tracking_settings_UF_R="
# --roa4=$regions_dir/manually_created/ROA_L_a4-40_s39-77_c2-46.nii.gz
# --seed=JHU-ICBM-labels-1mm:Uncinate_fasciculus_R
# --end=$regions_dir/from_sgranger/Granger_End_BA_11_47.nii.gz+Brodmann:BA10_R
# --seed_count=1000000 
# ";
# 
# 
# tracking_settings_UF_L="
# --roa4=$regions_dir/manually_created/ROA_R_a4-40_s0-39_c2-46.nii.gz
# --seed=JHU-ICBM-labels-1mm:Uncinate_fasciculus_L
# --end=$regions_dir/from_sgranger/Granger_End_BA_11_47.nii.gz+Brodmann:BA10_L
# --seed_count=1000000 
# ";
# 
# tracking_settings_sub_UF_R="
# --roa4=$regions_dir/manually_created/ROA_L_a4-40_s39-77_c2-46.nii.gz
# --seed=AAL2:OFCmed_R+AAL2:OFCant_R+AAL2:OFCpost_R+AAL2:OFClat_R
# --end=FreeSurferDKT_Cortical:right_entorhinal+FreeSurferDKT_Subcortical:right_amygdala
# --seed_count=10000000 
# ";
# 
# tracking_settings_sub_UF_L="
# --roa4=$regions_dir/manually_created/ROA_R_a4-40_s0-39_c2-46.nii.gz
# --seed=AAL2:OFCmed_L+AAL2:OFCant_L+AAL2:OFCpost_L+AAL2:OFClat_L
# --end=FreeSurferDKT_Cortical:left_entorhinal+FreeSurferDKT_Subcortical:left_amygdala
# --seed_count=10000000 
# ";
# 
# # fib_files=$(ls $test_src_dir/fib/*.fib.gz | xargs -n1 basename)
# fib_files=$(ls $src_dir/fib/*.fib.gz | xargs -n1 basename)
# 
# # mkdir -p $test_src_dir/trk
# mkdir -p $src_dir/trk
# 
# for fib_file in $fib_files 
# do
#     echo
#     echo $fib_file
#     dsi_studio --action=trk --source=$src_dir/fib/$fib_file $tracking_settings $tracking_settings_UF_L --output=$src_dir/trk/${fib_file}_UF_L.trk.gz
#     mv $src_dir/fib/${fib_file}.tt.gz $src_dir/trk/${fib_file}_UF_L.tt.gz ## has to be copied and renamed manually due to problems with the --output option
#     dsi_studio --action=trk --source=$src_dir/fib/$fib_file $tracking_settings $tracking_settings_UF_R --output=$src_dir/trk/${fib_file}_UF_R.trk.gz
#     mv $src_dir/fib/${fib_file}.tt.gz $src_dir/trk/${fib_file}_UF_R.tt.gz
#     dsi_studio --action=trk --source=$src_dir/fib/$fib_file $tracking_settings $tracking_settings_sub_UF_L --output=$src_dir/trk/${fib_file}_sub_UF_L.trk.gz
#     mv $src_dir/fib/${fib_file}.tt.gz $src_dir/trk/${fib_file}_sub_UF_L.tt.gz
#     dsi_studio --action=trk --source=$src_dir/fib/$fib_file $tracking_settings $tracking_settings_sub_UF_R --output=$src_dir/trk/${fib_file}_sub_UF_R.trk.gz
#     mv $src_dir/fib/${fib_file}.tt.gz $src_dir/trk/${fib_file}_sub_UF_R.tt.gz
#     echo
# done
# 
#  
/data/pt_02020/MRI_data/wd/DWI/src/fib/sub-02_ses-01_dwi.src.gz.odf.gqi.2.fib.gz
/data/pt_02020/MRI_data/wd/DWI/src/trk/sub-02_ses-01_dwi.src.gz.odf.gqi.2.fib.gz_sub_UF_L.tt.gz

# echo "#####"
# echo "Step 5: Tract-specific analysis"
# echo "#####"
# 
# mkdir -p $test_src_dir/ana
# mkdir -p $src_dir/ana
# 
# # fib_files=$(ls $test_src_dir/fib/*.fib.gz | xargs -n1 basename)
# fib_files=$(ls $src_dir/fib/*.fib.gz | xargs -n1 basename)
# 
# #     trk_files=$(ls $test_src_dir/trk/${fib_file}*.tt.gz | xargs -n1 basename)
#     trk_files=$(ls $src_dir/trk/*.tt.gz | xargs -n1 basename)
#     
# for fib_file in $fib_files 
# do
#         echo 
#         dsi_studio --action=ana --source=$src_dir/fib/$fib_file --tract=$src_dir/trk/${fib_file}_sub_UF_L.tt.gz --export=stat
#         dsi_studio --action=ana --source=$src_dir/fib/$fib_file --tract=$src_dir/trk/${fib_file}_sub_UF_R.tt.gz --export=stat
#         dsi_studio --action=ana --source=$src_dir/fib/$fib_file --tract=$src_dir/trk/${fib_file}_UF_L.tt.gz --export=stat
#         dsi_studio --action=ana --source=$src_dir/fib/$fib_file --tract=$src_dir/trk/${fib_file}_UF_R.tt.gz --export=stat
#         mv $src_dir/trk/${fib_file}*stat.txt $src_dir/ana/
#         echo
# done


echo "#####"
echo "Step 6: Copy data into results directory"
echo "#####"
mkdir -p $results_dir/tracts
mkdir -p $results_dir/analysis_of_tracts
# cp -r $src_dir/trk/* $results_dir/tracts/
cp -r $src_dir/ana/* $results_dir/analysis_of_tracts/
