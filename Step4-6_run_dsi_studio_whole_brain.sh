#! /bin/bash

# author: thieleking@cbs.mpg.de
#created on 18th Oct 21
# testing phase over on 2022-01-25 -> start of main analysis

### ### Step 0a:  connect to compute server with cuda support
# getserver -g6 -sL --cuda=x
### ### Step 0b: choose version, start shell and mount paths
# singularity shell --nv -B /data/pt_02020 /data/p_SoftwareServiceLinux_sc/dsistudio/2022.01.11_JHU-ICBM-labels-1mm/1

##### TODO: Define directories

##### originals directories
input_dir_eddy="/data/pt_02020/MRI_data/DWI_preprocessed/eddy" ## take nifti and bvecs from here: e.g. /data/pt_02020/MRI_data/DWI_preprocessed/eddy/sub-04/ses-02/eddy_corrected.nii.gz and /data/pt_02020/MRI_data/DWI_preprocessed/eddy/sub-04/ses-02/eddy_corrected.eddy_rotated_bvecs
input_dir_bvals="/data/pt_02020/MRI_data/BIDS" ## e.g. /data/pt_02020/MRI_data/BIDS/sub-04/ses-02/dwi/sub-04_ses-02_dwi.bval

#### working directories
input_dir="/data/pt_02020/MRI_data/wd/DWI/input_dsistudio"
src_dir="/data/pt_02020/MRI_data/wd/DWI/src"
exclusions_dir="/data/pt_02020/MRI_data/wd/DWI/excluded_src"

#### final results directory
results_dir="/data/pt_02020/MRI_data/DWI_results/tracking_whole_brain"


# echo "#####"
# echo "Step 4: Fiber tracking"
# echo "#####"
# 
# tracking_settings="
# --method=0 
# --threshold_index=qa 
# --fa_threshold=0.00 
# --initial_dir=0 
# --seed_plan=0 
# --interpolation=0 
# --thread_count=16 
# --step_size=0 
# --turning_angle=0
# --smoothing=0.6 
# --min_length=0 
# --max_length=300 
# --seed_count=10000000 
# ";
# 
# 
# fib_files=$(ls $src_dir/fib/*.fib.gz | xargs -n1 basename)
# 
# mkdir -p $src_dir/trk
# 
# for fib_file in $fib_files 
# do
#     echo
#     echo "-----------------------------" $fib_file "-----------------------------" 
#     echo
#     dsi_studio --action=trk --source=$src_dir/fib/$fib_file $tracking_settings --output=$src_dir/trk/${fib_file}_whole_brain.trk.gz
#     mv $src_dir/fib/${fib_file}.tt.gz $src_dir/trk/${fib_file}_whole_brain.tt.gz ## has to be copied and renamed manually due to problems with the --output option
#     echo
# done

 

echo "#####"
echo "Step 5: Tract-specific analysis"
echo "#####"

mkdir -p $src_dir/ana

fib_files=$(ls $src_dir/fib/*.fib.gz | xargs -n1 basename)
# fib_files="sub-13_ses-02_dwi.src.gz.odf.gqi.2.fib.gz" ## re-run due to error msg
# fib_files="sub-40_ses-04_dwi.src.gz.odf.gqi.2.fib.gz" ## re-run due to error msg


for fib_file in $fib_files 
do
    echo
    echo "-----------------------------" ${fib_file}_whole_brain.tt.gz "-----------------------------" 
    echo
    dsi_studio --action=ana --source=$src_dir/fib/$fib_file --tract=$src_dir/trk/${fib_file}_whole_brain.tt.gz --export=stat
    mv $src_dir/trk/${fib_file}_whole_brain*stat.txt $src_dir/ana/
    echo
done


# echo "#####"
# echo "Step 6: Copy data into results directory"
# echo "#####"
# mkdir -p $results_dir/tracts
# mkdir -p $results_dir/analysis_of_tracts
# cp -r $src_dir/trk/*_whole_brain* $results_dir/tracts/
# cp -r $src_dir/ana/*_whole_brain* $results_dir/analysis_of_tracts/
