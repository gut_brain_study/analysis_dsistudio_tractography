#! /bin/bash

# author: thieleking@cbs.mpg.de
#created on 1st Dec 21

echo "#####"
echo "Step 1: create src files"
echo "#####"

subjects=$(ls $input_dir)
for sub in $subjects
do
    sessions=$(ls $input_dir/$sub)
    for ses in $sessions
    do
        dsi_studio --action=src --source=$test_dir/$sub/$ses/*.nii.gz --bval=$test_dir/$sub/$ses/${sub}_${ses}_dwi.bval.txt --bvec=$test_dir/$sub/$ses/${sub}_${ses}_dwi.eddy_rotated_bvecs.txt --output=$test_src_dir --up_sampling=0
    done
done
        

# # NOTE:
# # output: assign the output src file name.
# # 
# # b_table: assign the replacement b-table
# # 
# # bval: assign the b value text file
# # 
# # bvec: assign the b vector text file
# # 
# # recursive: search files in the subdirectories. e.g. "--recursive=1".
# # 
# # up_sampling: upsampling the DWI. 
# # --up_sampling=0 : no upsampling
# # --up_sampling=1 : upsampling by 2
# # --up_sampling=2 : downsampling by 2
# # --up_sampling=3 : upsampling by 4
# # --up_sampling=4 : downsampling by 4

echo "#####"
echo "Step 2: Quality control"
echo "#####"

dsi_studio --action=qc --source=$src_dir

# # NOTE:
# # output is stored in source directory as qc.txt
# # 1st: check consistency of image dimension, resolution, DWI count, shell count. It is likely that certain scans are incomplete, and the total DWI count will be different.
# # 2nd: check "neighboring DWI correlation" which is the correlation coefficient of low-b DWI volumes that have similar gradient direction. Higher correlation indicates good quality. The value will decrease if there is a prominent eddy current artifact, head motion, or any head coil issues that may impact the diffusion signals. 
# # DSI Studio will use an outlier checking function (e.g. 3 median absolute deviations) to label problematic data with "low quality outlier." If a dataset has a very low neighboring DWI correlation, you will need to examine the raw DWI images to see if the problem is correctable. Otherwise, the problematic datasets should be excluded from analysis.

echo "#####"
echo "Step 3: Reconstruction using GQI"
echo "#####"

src_files=$(ls $src_dir/*.src.gz)

mkdir -p $src_dir/fib

for src_file in $src_files
do
    echo
    dsi_studio --action=rec --source=$src_file --method=4 --param0=2.1 --record_odf=1 --output=$src_dir/fib
    echo
done


# # NOTE:
# # method: assign the reconstruction methods. 0:DSI, 1:DTI, 2:Funk-Randon QBI, 3:Spherical Harmonic QBI, 4:GQI 6: Convert to HARDI 7:QSDR.
# # param0: ratio of mean diffusion distance; TODO: try multiple (from 0.2 to 3.0) and locate non-crossing regions (e.g. mid corpus callosum) and crossing regions (e.g. lateral corpus callosum). The optimal value should resolve crossing patterns in crossing regions and still have clean fiber direction at mid corpus callosum.
# # odf_order: assign the tesellation number of the odf. Supported values include 4, 5, 6, 8, 10, 12, 16, 20. The default value is 8.
# # num_fiber: the maximum count of the resolving fibers for each voxel, default=5.
# # scheme_balance: set "--scheme_balance=1" to enable scheme balance.
# # half_sphere: set "--half_sphere=1" if the data were acquired with half sphere DSI.
# # deconvolution: set "--deconvolution=1" to apply deconvolution. Use --param2=0.5 to assign the regularization parameter.
# # decomposition: set "--decomposition=1" to apply decomposition. Use --param3=0.05 to assign the decomposition fraction and --param4=10 to assign the m value.
# # r2 weighted: set "--r2_weighted=1" to apply r2-weighted GQI reconstruction.
# # other_src: assign the src file for correcting the phase distortion (e.g. --source=PA_scan.src.gz --other_src=AP_scan.src.gz). It does not matter whether the other src is AP or PA.
# # affine: specify the file name of a text file containing a transformation matrix. DWI and its b-table will be rotated according to the matrix. 
# #             An example of the matrix is the following (shift in x and y directions by 10 voxels)
# #             1 0 0 -10
# #             0 1 0 -10 
# #             0 0 1 0
# # rotate_to: specify a T1W or T1W to rotate DWI to its space.
# # motion_correction: set "--motion_correction=1" to apply motion and eddy current correction. This correction works only on DTI dataset.
# # interpo_method: assign the interpolation method used in QSDR. 0:trilinear 1:gaussian radial basis 2: tricubic interpolation
# # check_btable: set "--check_btable=0" to disable automatic b-table flipping.
# # other_image: assign other image volumes (e.g., T1W, T2W image) to be wrapped with QSDR. For example: --other_image=t1w:/directory/my_t1w.nii.gz,t2w:/directory/my_t1w.nii.gz
# # output_mapping: used in QSDR to output mapping for each voxel
# # output_jac: used in QSDR to output jacobian determinant
# # output_dif: used in DTI to output diffusivity (default is 1)
# # output_tensor: used in DTI to output the whole tensor
# # output_rdi: used in GQI to output restricted diffusion imaging
# # record_odf: set "--record_odf=1" to output the ODF for connectometry analysis.
# # csf_calibration: set "--csf_calibration=1" to enable CSF calibration in GQI
# # cmd: run commands to modify the default mask or flip image volume or b-table. Use "+" to combine a sequence of command, and use "=" to assign value/parameters for each command
# #             e.g. --cmd="[Step T2a][Threshold]=0+[Step T2a][Smoothing]+[Step T2a][defragment]+[Step T2][Edit][Image flip x]"
# #             The available commands include:
# #             [Step T2a][Smoothing]
# #             [Step T2a][Defragment]
# #             [Step T2a][Dilation]
# #             [Step T2a][Erosion]
# #             [Step T2a][Negate]
# #             [Step T2a][Remove Background]
# #             [Step T2a][Threshold]=100       // assign value you like        
# #             [Step T2][Edit][Image flip x]
# #             [Step T2][Edit][Image flip y]
# #             [Step T2][Edit][Image flip z]
# #             [Step T2][Edit][Image swap xy]
# #             [Step T2][Edit][Image swap yz]
# #             [Step T2][Edit][Image swap xz]
# #             [Step T2][Edit][Rotate to MNI]      // rotate to 1-mm MNI
# #             [Step T2][Edit][Rotate to MNI2]    // rotate to 2-mm MNI
# #             [Step T2][Edit][Trim]
# #             [Step T2][Edit][Change b-table:flip bx]
# #             [Step T2][Edit][Change b-table:flip by]
# #             [Step T2][Edit][Change b-table:flip bz]
# #             [Step T2b(2)][Compare SRC]=file_name.src.gz
# #             [Step T2][Edit][Overwrite Voxel Size]=1.0
# #             [Step T2][Edit][Resample]=1.0
# #    

echo "#####"
echo "Step 4: Fiber tracking"
echo "#####"

dsi_studio --action=trk --source=$src_dir/fib/*.fib.gz --seed_count=500000 --threshold_index=qa --fa_threshold=0 --initial_dir=0 --seed_plan=0 --interpolation=0 --step_size=0 --turning_angle=55 --smoothing=0.6 --min_lenth=0 -- max_length=150 --output=$src_dir/trk/UF_R.tt.gz --track_id=Uncinate_Fasciculus_R --seed=UF_R.nii.gz --roi=xxx.nii.gz --roa=xxx.nii.gz --export=stat,tdi

# # NOTE:
# # Fiber tracking parameters
# # 
# # method: tracking methods 0:streamline (default), 1:rk4 
# # fiber_count : specify the number of fibers to be generated. If seed number is preferred, use seed_count instead. If DSI Studio cannot find a track connecting the ROI, then the program may run forever. To avoid this problem, you may assign fiber_count and seed_count at the same time so that DSI Studio can terminate if the seed count reaches a large number.
# # fa_threshold(default=0, which means it is randomized): threshold for fiber tracking. In QBI, DSI, and GQI, "fa_threshold" will be applied to the QA threshold. To use other index as the threshold, add "threshold_index=[name of the index]" (e.g. "--threshold_index=nqa --fa_threshold=0.01" sets a threshold of 0.01 on nqa for tract termination). If fa_threshold is not assigned, then DSI Studio will select a random value between 0.5Otsu and 0.7 Otsu threshold using a uniform distribution.
# # otsu_threshold(default=0.6): The default Otsu's threshold can be adjusted to any ratio.
# # initial_dir(default=0): initial propagation direction 0:primary fiber, 1:random, 2:all fiber orientations
# # seed_plan(default=0): specify the seeding strategy 0:subvoxel random, 1:voxelwise center
# # interpolation (default=0):interpolation methods (0:trilinear, 1:gaussian radial, 2:nearest neighbor)
# # thread_count (default=CPU number): specify the thread count. 1 for a single thread. 2 for two threads...
# # random_seed (default=0): specify whether a timer is used for generating seed points. Setting it on (--random_seed=1) will make tracking random. The default is off. 
# # tip_iteration (deafult=16 if assigning --track_id or --dt_threshold_index)(default=0 otherwise): specify pruning iterations (mostly used for differential tractography).
# # step_size, turning_angle, interpo_angle, fa_threshold, smoothing, min_length, max_length: 
# # The step_size, min_length, and max_length are at a scale of a millimeter.
# # *Assigning these tracking parameters takes a lot of time. To save the hassle, you can use "parameter_id" from the GUI (After running fiber tracking in GUI, the parameter ID will be shown in the method text in the right bottom window) to override all parameters in one shot (e.g. --action=trk --source=some.fib.gz --parameter_id=c9A99193Fba3F2EFF013Fcb2041b96438813dcb). Please note that parameter ID does not override ROI settings, dt_threshold_index, or threshold_index settings. You may still need to assign ROI/ROA...etc. in the command line. 
# # track_id: specify which track to map using automatic fiber tracking. (e.g. --track_id=Arcuate_Fasciculus_L ) The value lookup list is the text file included in the DSI Studio package under the "track" folder.
# # 
# # ROI parameters
# # 
# # seed: specify the seeding file. Supported file format includes text files, Analyze files, and nifti files.
# # roi, roi2, roi3, roi4, roi5: specify the roi file. Supported file format includes text files, Analyze files, and nifti files. Multiple input is supported.
# #         You may use atlas regions as the roi, roa, end, or ter regions. Specify the atlas and region name (case sensitive!) as follows:
# #         --roi=FreeSurferDKT:left_precentral
# #         An ROI (and also other region types) can be modified by the following action code: "smoothing", "erosion", "dilation",  "defragment", "negate", "flipx", "flipy", "flipz", "shiftx" (shift the roi by 1 in x direction), "shiftnx" (shift the roi by -1 in x direction), "shifty", "shiftny", "shiftz", "shiftnz". 
# #         For example, --roi=region.nii.gz,dilate,dilate,smoothing dilates the region in the region.nii.gz file twice and smooth it:
# #         *You can assign the region value to be loaded (e.g. --roi=nifti.gz:1 only load regions with value=1)
# # roa, roa2, roa3, roa4, roa5: specify the roa. You may assign as most 5 roa regions.
# # end, end2: specify the endpoint. You may assign as most 2 end regions (e.g. --end2)
# # ter, ter2, ter3, ter4, ter5: specify the terminative region. You may assign as most 5 terminative regions.
# # t1t2: specify t1w or t2w images as the ROI reference image (e.g. --t1t2=my_t1.nii.gz)
# # dt_threshold_index: trigger differential tractography. Assign "dec_qa" for finding decreased connectivity, "inc_qa for finding increased connectivity    
# # dt_threshold: assign percentage change for differential tractography. assign --dt_threshold=0.1 to detect more than 10% change.
# # 
# # Post-processing parameters
# # 
# # The following is a list of the track post-processing command (can also be used in --action=ana without fiber tracking)
# # delete_repeat: assign the distance for removing repeat tracks (e.g. --delete_repeat=1 removes repeat tracks with distance smaller than 1 mm)
# # output: specify the output directory or output file name. Supported file format includes .tt.gz file or .trk.gz file. You canset this parameter to no_file to disable tractography output. You may also assign the .nii file as the output. DSI Studio will convert tracks into a region and save them in nifti file format. 
# # You can assign multiple output files (e.g. output=track.tt.gz,track.nii.gz,track.txt)
# # end_point: output endpoint as a txt file or mat file. specify the file name using --end_point=file_name.txt
# # export: export along-track indices, statistics, TDI, or track analysis report. See the export option documented under --action=ana (below) for detail.
# # connectivity: output connectivity matrix using ROIs or atlas as the matrix entry. 
# #     (1) If you would like to use the built-in atlas:
# #     Please specify the atlas name. For example, "--connectivity=FreeSurferDKT" uses FreeSurferDKT atlas. You can assign multiple atlases (e.g., --connectivity=FreeSurferDKT,HCP-MMP
# #     (2) If you would like to use your customized MNI space atlas:
# #     Check out here about how to install a customized MNI space atlas. Then specify the name of the atlas without adding the file extension (e.g., --connectivity=mni_space_atlas)
# #     (3) If you would like to use subject space ROIs:       
# #     Please specify the full file name of the atlas file (e.g., --connectivity=/subject01/segmentation_in_dwi_space.nii.gz)
# #     If the ROIs is segmented based on T1W, you will need to specify the original t1w file using --t1t2 (e.g., --t1t2=subject_t1w.nii.gz) 
# #     Otherwise, make sure the ROI has the same dimension as the DWI so that DSI Studio can use it.
# # connectivity_type: specify whether to use "pass" or "end" to count the tracks. The default setting is "end". 
# # connectivity_value: specify the way to calculate the matrix value. The default is "count", which means the number of tracks passing/ending in the regions. "ncount" is the number of tracks normalized by the median length. "mean_length" outputs the mean length of the tracks. "trk" outputs a trk file each connectivity matrix entry. Other options include "fa" (if DTI reconstruction is used), "qa", "adc" (if DTI reconstruction is used). The name of the scalar values can be found by opening the FIB file in STEP fiber tracking. There will be a list of scalar value in the region window to the left. 
# # You can output multiple results by separating the parameters with ",". (e.g. --connectivity_value=count,ncount,trk)
# # Please note that if the number of the connecting tracks are not enough (determined by connectivity_threshold), the connectivity value will not be calculated. This strategy is used to avoid inclusion of accidental connections due to false tracks.
# # connectivity_threshold: specify the threshold for calculating binarized graph measures and connectivity values. The default value is 0.001. This means if the maximum connectivity count is 1000 tracks in the connectivity matrix, then at least 1000 x 0.001 = 1 track is needed to pass the threshold. Otherwise, the values will be set to zero.
# # ref: output track coordinate based on a reference image (e.g. T1w or T2w).
# # cluster: run track clustering after fiber tracking. 4 parameters separated by comma are needed: --cluster=METHOD_ID,CLUSTER_COUNT,RESOLUTION,OUTPUT FILENAME
# #                 METHOD_ID: 0=single-linkage clustering 1=k means 2=EM
# #                 CLUSTER_COUNT: The total number of clusters. In k-means or EM, this is the total number of clusters assigned. In single-linkage, it is the maximum number of clusters allowed to avoid over-segmentation (the remaining small clusters will be group in the last clusters).
# #                 RESOLUTION: only used in single-linkage clustering. The value is the mini meter resolution for merging the clusters.
# #                 OUTPUT FILENAME: the file name for output the cluster label (should be a txt file). The file name should not contain a space.
# #                 Example: --cluster=0,500,2,label.txt  run a single-linkage cluster with 500 maximum cluster count and 2-mm resolution, saved as label.txt
# # other_slices: add other imaging metrics (e.g, --other_slices=DKI.nii.gz,FW_FA.nii.gz,ODI.nii.gz or --other_slices=./other/*.nii.gz) and report them with --export or --connectivity

echo "#####"
echo "Step 5: Tract-specific analysis"
echo "#####"

#e.g. Get track statistics from a track file:

dsi_studio --action=ana --source=my.fib.gz --tract=tract.tt.gz --export=stat

# # NOTE:
# Parameters
# 
# action: use "ana" for analysis. The output of the results will be stored in the same directory of the tract file.
# source: assign the fib.gz file.
# The following setting applies to tract-specific analysis:
# tract: assign the tract file (*.tt.gz *.trk.gz *.tck).
# output: use"--output=Tract.txt" to convert trk file to other format or ROI (assigned output file as *.nii.gz)
# 
# export: export additional information related to the fiber tracts 
#         use "--export=tdi" to generate track density image in the diffusion space.
#         use "--export=tdi2" to generate track density image in the subvoxel diffusion space.
#         use "--export=tdi_color" or "--export=tdi2_color" to generate track color density image.
#         use "--export=stat" to export tracts statistics like along tract mean fa, adc, or morphology index such as volume, length, ... etc.
#         To export TDI endpoints, use tdi_end or tdi2_end.
#         use "--export=report:dti_fa:0:1" to export the tract reports on "fa" values with a profile style at x-direction "0" and a bandwidth of "1" 
#         the profile style can be the following:
# 
#         0 x-direction
#         1 y-direction
#         2 z-direction
#         3 along tracts
#         4 mean of each tract
# 
#         for detail of each profile style, please refer to the following link: http://dsi-studio.labsolver.org/Manual/tract-specific-analysis
# 
#         You can export multiple outputs separated by ",". For example, 
#         --export=stat,tdi,tdi2 exports tract statistics, tract density images (TDI), subvoxel TDI, along tract qa values, and along tract gfa values.
# 
# The following are post-processing parameters:
# The "ana" action is compatible with the track post-processing commands listed under "--action=trk", including "delete_repeat","output", "export", "end_point", "ref", "connectivity", "connectivity_type", "connectivity_value", and ROI related commands, such as "roi", "roi2", "roi3", "roi4", "roi5", "roa", "roa2", "roa3", "roa4", "roa5", "end", "end2", and "ter". Please check out "--action=trk" for details.
#
# The following setting applies to region-based analysis: 
# Do not assign "--tract", or DSI Studio will run tract-specific analysis.
# region: regions: assign the file name(s) of a single ROI (--region) or a nifti file of multiple ROIs (--regions). 
# Different files are separated by "+". The format can be a txt file or nifti file or from the atlas regions (e.g. --region=FreeSurferDKT:right_precentral+FreeSurferDKT:left_precentral)
# If the regions are derived from T1W or T2W, please assign the original T1W/T2W using --t1t2=t1w.nii.gz
# If the regions are in the MNI space, add "mni" to the file name (e.g. mni_regions.nii.gz)
# The loaded regions can be modified using "smoothing", "erosion", "dilation",  "defragment", "negate", "flipx", "flipy", "flipz", "shiftx" (shift the roi by 1 in x direction), "shiftnx" (shift the roi by -1 in x direction), "shifty", "shiftny", "shiftz", "shiftnz". 
# For example, --region=region.nii.gz,dilate,smoothing --regions=multiple_regions.nii.gz,flipx
# regions: assign the nifti file of multiple ROI for analysis. You may use --t1t2 to assign the original T1W/T2W for registration with the DWI.
# atlas: assign the name of an atlas to provide a set of ROI to export the diffusion indices (e.g, atlas=FreeSurferDKT)
# export: use "--export=stat" to export region statistics
